cmake_minimum_required(VERSION 2.8)

project(qbsp2)

option(USE_GLDRAW "Use the gldraw code, this needs GLAUX" OFF)

### Find OpenGL
find_package(OpenGL REQUIRED)

add_executable(qbsp2
    ../common/bsplib.c
    ../common/bsplib.h
    ../common/cmdlib.c
    ../common/mathlib.c
    ../common/scriplib.c
    ../common/threads.c
    ../common/cmdlib.h
    ../common/mathlib.h
    ../common/scriplib.h
    ../common/threads.h
    bsp5.h
    merge.c
    outside.c
    portals.c
    qbsp.c
    solidbsp.c
    surfaces.c
    tjunc.c
    writebsp.c
)

if (USE_GLDRAW)
    target_sources(qbsp2
        PRIVATE
            gldraw.c
    )

    target_compile_definitions(qbsp2
        PUBLIC
            USE_GLDRAW
    )

    target_link_libraries(qbsp2
        PRIVATE
            ${OPENGL_LIBRARIES}
    )
endif (USE_GLDRAW)

target_include_directories(qbsp2
    PRIVATE
        "../common"
)

target_compile_definitions(qbsp2
    PUBLIC
        DOUBLEVEC_T
)

cmake_minimum_required(VERSION 2.8)

project(vis)

add_executable(vis
    ../common/bsplib.c
    ../common/bsplib.h
    ../common/cmdlib.c
    ../common/mathlib.c
    ../common/scriplib.c
    ../common/threads.c
    ../common/threads.h
    ../qbsp2/bsp5.h
    ../common/bspfile.h
    ../common/cmdlib.h
    ../common/mathlib.h
    ../common/scriplib.h
    flow.c
    soundpvs.c
    vis.c
    vis.h
)

target_include_directories(vis
    PRIVATE
        "../common"
)

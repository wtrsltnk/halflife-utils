cmake_minimum_required(VERSION 3.8)

project(sprgen)

add_executable(sprgen
    ../common/cmdlib.c
    ../common/lbmlib.c
    ../common/scriplib.c
    ../common/cmdlib.h
    ../common/lbmlib.h
    ../common/scriplib.h
    sprgen.c
    spritegn.h
)

target_include_directories(sprgen
    PRIVATE
        "../common"
)
